### **PT-BR**
### **olá!, eu sou PSousaDev,Estudante assiduo do mundo do desenvolvimento**👋

[![instagram](https://img.shields.io/badge/Instagram-E4405F?style=for-the-badge&logo=instagram&logoColor=white)](https://www.instagram.com/_pedro.si/)[![disc](https://img.shields.io/badge/LinkedIn-0077B5?style=for-the-badge&logo=linkedin&logoColor=white)](https://www.linkedin.com/in/pedrosi/)

![pedrosi stats](https://github-readme-stats.vercel.app/api?username=PedroSousa&show_icons=true&theme=dracula)


# **Linguagens que Trabalho**:
<div style="display:inline-block"><BR\>
 <img src="https://img.shields.io/badge/HTML5-E34F26?style=for-the-badge&logo=html5&logoColor=white">   
</div>
<div style="display:inline-block"><BR\>
 <img src="https://img.shields.io/badge/Python-3776AB?style=for-the-badge&logo=python&logoColor=white">
</div>
<div style="display:inline-block"><BR\>
 <img src="https://img.shields.io/badge/CSS3-1572B6?style=for-the-badge&logo=css3&logoColor=white">
</div>
<div style="display:inline-block"><BR\>
 <img src="https://img.shields.io/badge/JavaScript-323330?style=for-the-badge&logo=javascript&logoColor=F7DF1E">
</div>
<div style="display:inline-block"><BR\>
 <img src="https://img.shields.io/badge/React-20232A?style=for-the-badge&logo=react&logoColor=61DAFB">
</div>
<div style="display:inline-block"><BR\>
 <img src="https://img.shields.io/badge/React-20232A?style=for-the-badge&logo=react&logoColor=61DAFB">
</div>
<div style="display:inline-block"><BR\>
 <img src="https://img.shields.io/badge/markdown-%23000000.svg?style=for-the-badge&logo=markdown&logoColor=white">
</div>

## **Frameworks que Utilizo**:

<div style="display:inline-block"><BR\>
 <img src="https://img.shields.io/badge/React-20232A?style=for-the-badge&logo=react&logoColor=61DAFB">
</div>
<div style="display:inline-block"><BR\>
 <img src="https://img.shields.io/badge/react_native-%2320232a.svg?style=for-the-badge&logo=react&logoColor=%2361DAFB">
</div>
<div style="display:inline-block"><BR\>
 <img src="https://img.shields.io/badge/tailwindcss-%2338B2AC.svg?style=for-the-badge&logo=tailwind-css&logoColor=white">
</div>
<div style="display:inline-block"><BR\>
 <img src="https://img.shields.io/badge/node.js-6DA55F?style=for-the-badge&logo=node.js&logoColor=white">
</div>
<div style="display:inline-block"><BR\>
 <img src="https://img.shields.io/badge/bootstrap-%23563D7C.svg?style=for-the-badge&logo=bootstrap&logoColor=white">
</div>
<div style="display:inline-block"><BR\>
 <img src="https://img.shields.io/badge/bulma-00D0B1?style=for-the-badge&logo=bulma&logoColor=white">
</div>
